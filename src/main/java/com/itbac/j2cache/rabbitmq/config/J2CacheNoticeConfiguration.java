package com.itbac.j2cache.rabbitmq.config;

import com.itbac.j2cache.rabbitmq.constants.ExchangeName;
import com.itbac.j2cache.rabbitmq.constants.RedisQueueName;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;


/**
 * @description 二级缓存MQ通知配置
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
//@Configuration  //继承自动配置类，不需要注解了。
public class J2CacheNoticeConfiguration extends RabbitAutoConfiguration {

    @Resource
    private RedisQueueName redisQueueName;

    @Bean
    public FanoutExchange j2cacheNoticeExchange() {
        //广播 交换器
        return new FanoutExchange(ExchangeName.j2cacheNotice);
    }

    @Bean
    public Queue j2cacheNoticeQueue() {
        //非持久化的,不排它，自动删除
        return new Queue(redisQueueName.getName(),
                false, false, true);
    }

    @Bean
    public Binding binding(FanoutExchange j2cacheNoticeExchange, Queue j2cacheNoticeQueue) {
        return BindingBuilder.bind(j2cacheNoticeQueue).to(j2cacheNoticeExchange);
    }

}
