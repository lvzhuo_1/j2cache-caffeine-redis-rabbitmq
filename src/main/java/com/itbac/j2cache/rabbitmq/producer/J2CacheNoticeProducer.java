package com.itbac.j2cache.rabbitmq.producer;

import com.google.gson.Gson;
import com.itbac.j2cache.rabbitmq.constants.ExchangeName;
import com.itbac.j2cache.rabbitmq.message.J2CacheNotice;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description 生产者
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@Component
public class J2CacheNoticeProducer {

    @Autowired()
    private RabbitTemplate rabbitTemplate;
    private Gson gson = new Gson();

    public void publish(J2CacheNotice j2CacheNotice) {
        //发送消息
        rabbitTemplate.convertAndSend(
                ExchangeName.j2cacheNotice, "", gson.toJson(j2CacheNotice));
    }


}
